const { GraphQLScalarType } = require('graphql')
const Query = require('./Query')

var _id = 0
var users = [
    { githubLogin: 'mHattrup', name: 'Mike Hattrup' },
    { githubLogin: 'gPlake', name: 'Glen Plake' },
    { githubLogin: 'sSchmidt', name: 'Scot Schmidt' }
]
var photos = [
    {
        id: '1',
        name: 'Dropping the Heart Chute',
        description: 'The heart chute is one of my favorite chutes',
        category: 'ACTION',
        githubUser: 'gPlake',
        created: '3-28-1977'
    },
    {
        id: '2',
        name: 'Enjoying the sunshine',
        category: 'SELFIE',
        githubUser: 'sSchmidt',
        created: '1-2-1985'
    },
    {
        id: '3',
        name: 'Gunbarrel 25',
        description: '25 laps on gunbarrel today',
        category: 'LANDSCAPE',
        githubUser: 'sSchmidt',
        created: '2018-04-15T19:09:57.308Z'
    }
]
var tags = [
    { photoID: '1', userID: 'gPlake' },
    { photoID: '2', userID: 'sSchmidt' },
    { photoID: '2', userID: 'mHattrup' },
    { photoID: '2', userID: 'gPlake' }
]

const resolvers = {
    Query,

    Mutation: {
        postPhoto(parent, args) {
            var newPhoto = { id: _id++, ...args.input, created: new Date() }
            photos.push(newPhoto)
            return newPhoto
        }
    },

    Photo: {
        url: ({ _id }) => `http://yoursite.com/img/${_id}.jpg`,
        postedBy: ({ githubUser }) =>
            users.find(({ githubLogin }) => githubLogin === githubUser),
        taggedUsers: ({ id }) =>
            tags
                .filter(({ photoID }) => photoID === id)
                .map(({ userID }) =>
                    users.find(({ githubLogin }) => githubLogin === userID)
                )
    },

    User: {
        postedPhotos: ({ githubLogin }) =>
            photos.filter(({ githubUser }) => githubUser === githubLogin),
        inPhotos: ({ githubLogin }) =>
            tags
                .filter(({ userID }) => userID === githubLogin)
                .map(({ photoID }) => photos.find(({ id }) => id === photoID))
    },

    DateTime: new GraphQLScalarType({
        name: 'DateTime',
        description: 'A valid date time value.',
        parseValue: (value) => new Date(value),
        serialize: (value) => new Date(value).toISOString(),
        parseLiteral: (ast) => ast.value
    })
}

module.exports = resolvers
