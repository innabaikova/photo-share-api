module.exports = {
    totalPhotos: (parent, args, { db }) =>
        db.collection('photos').estimatedDocumentCount(),
    allPhotos: (parent, args, { db }) =>
        db.collection('photos').find().toArray(),
    totalUsers: (parent, args, { db }) =>
        db.collection('users').estimatedDocumentCount(),
    allUsers: (parent, args, { db }) =>
        db.collection('users').find().toArray(),
    allPhotosAfter: async (parent, { after }, { db }) => {
        // TODO: Change when created will be actual Date
        const photos = await db
            .collection('photos')
            .find()
            .toArray()
        return photos.filter(({ created }) => new Date(created) > new Date(after))
    }
}